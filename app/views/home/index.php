
<!-- <div class="container"> -->
    <div class="container">
        <!-- section 1 -->
        <div class="row">
            <div class="col-xl-7 p-5">
            <img class="mt-5 " src="<?= BASE_URL;?>/img/image3.png" alt="" width="400">
                <p class="mt-4">VegetableShop adalah portal layanan belanja kebutuhan rumah online untukmu. Ucapkan selamat tinggal ke antrian panjang, toko yang padat, & kemacetan!</p>
                <p class="mt-4">Tambahkan sayuran segar, perlengkapan rumah tangga, dan lainnya ke troli dan pesananmu bisa dikirimkan di hari yang sama.</p>   
                <div class="btn btn-success">
                    Belanja Sekarang
                </div>
            </div>
            <div class="col-xl-5">
             <img class="m-5 " src="<?= BASE_URL;?>/img/image2.png" alt="" width="300">
            </div>
        </div>


        <!-- section 2 -->
        <div class="container row">
            <h4 class="text-center m-5">Mengapa Berbelanja dengan VegetableShop?</h4>
            <div class="col-xl-4">
                <img class="ms-5 mt-3" src="<?= BASE_URL;?>/img/img3.png" alt="" width="150">
                <h5 class="mt-4">Pilihan toko yang beragam</h5>
                <p>Beli kebutuhan rumah online dari berbagai supermarket ternama seperti AEON, Giant, Farmers Market, LotteMart, Big C, Tesco Lotus, Watsons dan masih banyak lagi!</p>
            </div>
            <div class="col-xl-4">
                <img class="ms-5" src="<?= BASE_URL;?>/img/img4.png" alt="" width="150">
                <h5 class="mt-3">Belanja kebutuhan online sesuai keinginanmu</h5>
                <p>Masukkan catatan dan chat dengan personal shopper & rider untuk pengalaman belanja yang mudah..</p>
            </div>
            <div class="col-xl-4">
                <img class="ms-5 mt-3" src="<?= BASE_URL;?>/img/img5.png" alt="" width="150">
                <h5 class="mt-3">Pembayaran non-tunai yang aman</h5>
                <p>Bayar dengan kartu, e-wallet dan beragam metode pembayaran lainnya yang aman.</p>
            </div>
        </div>
    </div>
    <!-- </div> -->

<!-- <style>
    .banner {
        min-height: 100vh;
        background-image: url('<?= BASE_URL; ?>/img/pohon.jpeg');
        background-size: cover;
        background-position: center;
    }
</style> -->