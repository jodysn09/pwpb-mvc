<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$data["judul"];?></title>
    <!-- <link rel="stylesheet" href="http://localhost/belajarmvc/public/css/bootstrap.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    </link>
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid ">
        <img src="img/Group 12.png" class="img-thumbnail ms-3" alt="..." width="200px">
            <button class="navbar-toggler " type="button"  data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse row " id="navbarNav">
                <ul class="navbar-nav col-lg-10 d-flex justify-content-center">
                    <li class="nav-item ">
                        <a class="nav-link active text-dark" aria-current="page" href="<?=BASE_URL;?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark" href="<?=BASE_URL;?>/about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark" href="<?=BASE_URL;?>/blog">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark " href="<?=BASE_URL;?>/user/profile">User</a>
                    </li>
                </ul>
                <div class="col-lg-1 mt-2">
                <?php if (isset($_SESSION['login'])) : ?>
                        <button type="logout" class="btn btn-succes" id="logout-button"><a href="<?= BASE_URL; ?>/Login/logout" class="text-light text-decoration-none">Login</a></button>
                    <?php else : ?>
                        <button type="login" class="btn btn-success btn-user" ><a href="<?= BASE_URL; ?>/login" class="text-light text-decoration-none">logout</a></button>
                    <?php endif; ?>
                    </div>
                    
            </div>
        </div>
    </nav>