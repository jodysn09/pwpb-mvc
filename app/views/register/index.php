<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$data["judul"];?></title>
    <link rel="stylesheet" href="http://localhost/belajarmvc/public/css/bootstrap.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>

<div class="container col-lg-6">
    <div class="row justify-content-center mt-5">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5 ">
                <div class="card-body p-0">
                    <div class="row">
                        <!-- <div class="col-lg-6 d-none d-lg-block bg-active"></div> -->
                        <!-- <div class="col-lg-6"> -->
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                </div>
                                <form class="user" method="post" action="<?=BASE_URL;?>/register/prosesRegister">
                                    <div class="form-group row">
                                        <label for="username" class="col-lg-4">Username</label>
                                        <input type="text" class="form-control form-control-user col-lg-8 mt-2" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Username" name="username" required>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-lg-4">Email</label>
                                        <input type="text" class="form-control form-control-user col-lg-8 mt-2" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Email" name="email" required>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password" class="col-lg-4 mt-2">Password</label>
                                        <input type="password" class="form-control form-control-user col-lg-8 mt-2" id="exampleInputPassword" placeholder="Password" name="password" required>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Cpassword" class="col-lg-5 mt-2">Confirmasi Password</label>
                                        <input type="password" class="form-control form-control-user col-lg-8 mt-2" id="exampleInputPassword" placeholder="Password" name="Cpassword" required>
                                    </div>
                                    <!-- <a href="" class="btn btn-primary btn-user btn-block mt-2">
                                        Login
                                    </a> -->
                                    <div class="button d-flex justify-content-center">
                                    <button type="submit" class="  btn btn-outline-success btn-user mt-2 ">Sign Up</button>
                                    </div>

                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="<?=BASE_URL?>/login">Sign In</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="http://localhost/belajarmvc/public/js/bootstrap.js"></script>

</body>
</html>







