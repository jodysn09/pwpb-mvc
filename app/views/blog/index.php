<div class="container mt-5"> 
    <div class="row"> 
        <div class="col-6"> 
            <h3>Blog</h3> 
            <ul class="list-group">
            <?php foreach ($data["blog"] as $blog) : ?> 
                <li class="list-group-item"><?= $blog['penulis']; ?></li>
            <?php endforeach; ?>
            </ul>
        </div> 
    </div> 
</div>
